#后期用于放类
import pygame
import random
import time
from src.interface import *

#蛇身贴图
snake_head = pygame.image.load('image/snake/snakehead.png')
snake_body1 = pygame.image.load('image/snake/snakebody1.png')
snake_body2 = pygame.image.load('image/snake/snakebody2.png')
snake_tail = pygame.image.load('image/snake/snaketail.png')

#剩下的蛇身使用旋转和翻转实现
snakebodyform = [
            #蛇头贴图
            [
                snake_head,
                pygame.transform.flip(snake_head, True, False),    #朝上右方向为基准
                pygame.transform.rotate(snake_head, DOWN),
                pygame.transform.rotate(pygame.transform.flip(snake_head, True, False), DOWN),
                pygame.transform.rotate(pygame.transform.flip(snake_head, True, False), LEFT),
                pygame.transform.rotate(snake_head, LEFT),
                pygame.transform.rotate(pygame.transform.flip(snake_head, True, False), RIGHT), 
                pygame.transform.rotate(snake_head, RIGHT),
            ],

            #蛇身贴图（直行）
            [
                pygame.transform.rotate(pygame.transform.flip(snake_body1, False, True), RIGHT),
                pygame.transform.rotate(snake_body1, RIGHT), 
                snake_body1,
                pygame.transform.flip(snake_body1, True, False),
            ],

            #蛇身贴图（弯曲）
            [
                pygame.transform.rotate(pygame.transform.flip(snake_body2, True, False), LEFT),
                pygame.transform.rotate(pygame.transform.flip(snake_body2, True, False), DOWN),
                pygame.transform.rotate(pygame.transform.flip(snake_body2, True, False), RIGHT), 
                pygame.transform.flip(snake_body2, True, False),
                pygame.transform.rotate(snake_body2, DOWN),
                pygame.transform.rotate(snake_body2, RIGHT), 
                snake_body2,
                pygame.transform.rotate(snake_body2, LEFT),
            ],

            #蛇尾贴图
            [
                pygame.transform.rotate(snake_tail, RIGHT),
                pygame.transform.rotate(pygame.transform.flip(snake_tail, True, False), LEFT),
                pygame.transform.rotate(snake_tail, LEFT),
                pygame.transform.rotate(pygame.transform.flip(snake_tail, True, False), RIGHT), 
                pygame.transform.rotate(pygame.transform.flip(snake_tail, True, False), DOWN),
                snake_tail,
                pygame.transform.flip(snake_tail, True, False),
                pygame.transform.rotate(snake_tail, DOWN),
            ]
        ]

DirtMap = {
    (1, 1 - SCREEN_X_BLOCK):(1, 1),
    (1, SCREEN_X_BLOCK - 1):(1, -1),
    (-1, 1 - SCREEN_X_BLOCK):(-1, 1),
    (-1, SCREEN_X_BLOCK - 1):(-1, -1),
    (SCREEN_Y_BLOCK - 1, 1):(-1, 1),
    (1 - SCREEN_Y_BLOCK, 1):(1, -1),
    (SCREEN_Y_BLOCK - 1, -1):(-1, -1),
    (1 - SCREEN_Y_BLOCK, -1):(1, -1),
            }

# 蛇类
# 点以25为单位
class Snake(object):
    # 初始化各种需要的属性 [开始时默认向右/身体块x5/蛇速x5/分数0/生命1]
    def __init__(self):
        self.dirction = pygame.K_RIGHT
        self.skills = []
        self.body = []
        self.speed = 5
        self.score = 0
        self.life = 0
        
        for x in range(4):   #初始长度为4
            self.addnode()
    
    def adjust(self,body):
        if body.top >= SCREEN_Y:
            body.top = 0
            return True
        elif body.top < 0:
            body.top = SCREEN_Y - 25
            return True
        if body.left >= SCREEN_X:
            body.left = 0
            return True
        elif body.left < 0:
            body.left = SCREEN_X - 25
            return True
        return False

    # 无论何时 都在前端增加蛇块
    def addnode(self):
        left,top = (0, 25)
        if self.body:
            left,top = (self.body[0].left,self.body[0].top)
        node = pygame.Rect(left,top,25,25)
        if self.dirction == pygame.K_LEFT:
            node.left -= 25
        elif self.dirction == pygame.K_RIGHT:
            node.left += 25
        elif self.dirction == pygame.K_UP:
            node.top -= 25
        elif self.dirction == pygame.K_DOWN:
            node.top += 25

        if "blood" in self.skills:
            if self.adjust(node):   #校正头放的位置
                sod_cros.play()
                self.skills.remove("blood")

        self.body.insert(0,node)
        
    # 删除最后一个块
    def delnode(self):
        self.body.pop()
    
    def addlife(self):
        self.life += 1
    
    def dellife(self):
        self.life -= 1
        
    # 死亡判断
    def isdead(self, wall):
        #体长
        if len(self.body) < 2:
            #self.addnode()
            return True

        # 撞边界
        if (self.body[0].x not in range(SCREEN_X)) or (self.body[0].y not in range(SCREEN_Y)):
            return True
            
        # 撞自己
        if not Casualmode():
            if self.body[0] in self.body[1:]:
                return True
        else:
            if self.body[0] in wall.wallpos:    #撞墙的时候判断是否有命
                if "blood" in self.skills:
                    sod_cros.play()
                    self.skills.remove("blood")
                    return False
                return True
        
        return False
        
    # 移动！
    def move(self):
        self.addnode()
        self.delnode()
        
    # 改变方向 但是左右、上下不能被逆向改变
    def changedirection(self,curkey):
        LR = [pygame.K_LEFT,pygame.K_RIGHT]
        UD = [pygame.K_UP,pygame.K_DOWN]
        if curkey in LR+UD:
            if (curkey in LR) and (self.dirction in LR):
                return
            elif (curkey in UD) and (self.dirction in UD):
                return
            self.dirction = curkey

#画蛇身
def show_snake(screen, snake):
    tmp = { True:1, False:0}

    tmpDir = {}
    Location = 0    #身体部位
    Direction = 0   #方向
    for index in range(len(snake.body)):
        curTop = snake.body[index].top
        curLeft =snake.body[index].left

        #判断是否为蛇头
        if 0 == index:
            Location = 0
            if snake.dirction == pygame.K_DOWN or snake.dirction == pygame.K_UP:  #竖向运动
                Direction = (snake.dirction - pygame.K_UP) * 2 + (curTop // 25 + curLeft % 2 + 1) % 2
            else:
                Direction = (snake.dirction - pygame.K_UP) * 2 + (curLeft // 25 + curTop % 2) % 2
            
        #判断是否为蛇尾
        elif len(snake.body) == (index + 1):
            befTop = snake.body[index-1].top
            befLeft = snake.body[index-1].left
            Location = 3
            if befTop == curTop:   #top 相同表示横向运动
                if abs(befLeft - curLeft) == 25:    #穿越边界防止尾巴方向相反
                    Direction = (tmp[befLeft < curLeft]+2) * 2 + (curLeft // 25 + curTop % 2) % 2
                else:
                    Direction = (tmp[not befLeft < curLeft]+2) * 2 + (curLeft // 25 + curTop % 2) % 2
            else:           #表示竖向运动
                if abs(befTop - curTop) == 25:
                    Direction = (tmp[befTop > curTop]) * 2 + (curTop // 25 + curLeft % 2 + 1) % 2
                else:
                    Direction = (tmp[not befTop > curTop]) * 2 + (curTop // 25 + curLeft % 2 + 1) % 2
        #非蛇头蛇尾
        else:
            #蛇身根据前后判断方向和转弯
            if snake.body[index-1].top == curTop and curTop == snake.body[index+1].top:    #横向运动
                Location = 1
                Direction = (curLeft // 25 + curTop % 2) % 2 + 2
            elif snake.body[index-1].left == curLeft and curLeft == snake.body[index+1].left:    #竖向运动
                Location = 1
                Direction = (curTop // 25 + curLeft % 2 + 1) % 2
            else:
                Location = 2
                dirc = ((snake.body[index + 1].top-snake.body[index-1].top) // 25,
                        (snake.body[index + 1].left-snake.body[index-1].left) // 25)
                ret=[dirc, snake.body[index -1].top==curTop, snake.body[index - 1].left==curLeft]
                posi = 4 * ((curTop**2 + curLeft**2) % 2)

                if ret[0] in DirtMap:
                    ret[0] = DirtMap[ret[0]]

                #右上角或左下角
                if ret[0] == (1, 1):    
                    #逆时针
                    if ret[1]:
                        Direction = 1 + posi
                    #顺时针
                    else:
                        Direction = 3 + posi    #  3 7
                #右上角或左下角
                elif ret[0] == (-1, -1):
                    if not ret[1]:
                        Direction =1 + posi
                    else:
                        Direction = 3 + posi
                #左上角或右下角
                elif ret[0] == (-1, 1):
                    if ret[2]:
                        Direction = posi       #0 4
                    else:
                        Direction = 2 + posi
                #左上角或右下角
                elif ret[0] == (1, -1):
                    if not ret[2]:
                        Direction =  posi
                    else:
                        Direction = 2 + posi
                else:
                    print("Error:", ret)
                    pass
        screen.blit(snakebodyform[Location][Direction], snake.body[index])