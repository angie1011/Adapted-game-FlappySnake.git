#本文件主要是蛇吃的食物类
import pygame
import sys
import random
import time
from src.interface import *

foods = {
    "insect" : pygame.image.load('image/insect.png'),
    "rocket" :pygame.image.load('image/rocket.png'),
    "snails" :pygame.image.load('image/snails.png'),
    "bird": pygame.image.load('image/bird.png'),
    "medicine": pygame.image.load('image/medicine.png'),
    "knife": pygame.image.load('image/knife.png'),
    "blood": pygame.image.load('image/blood.png'),
    
}

# 食物类
# 方法： 放置/移除
# 点以25为单位
class Food:
    def __init__(self):
        self.rect = pygame.Rect(-25,0,25,25)
        self.foodtype = "insect"
        self.allfoodtype = list(foods.keys())
        self.allfoodtype.remove("bird")

    def remove(self):
        self.rect.x=-25
        
    #设置食物类型
    def setfoodtype(self, snake):
        while True:
            self.foodtype = random.choice(self.allfoodtype)
            if self.foodtype == "knife" and len(snake.body) < 4:
                continue
            else:
                break
            
    def set(self, wall):
        if self.rect.x == -25:
            while True:
                Xpos = []
                Ypos = []
                # 不靠墙太近 25 ~ SCREEN_X-25 之间
                for pos in range(25,SCREEN_X-25,25):
                    Xpos.append(pos)
                self.rect.left = random.choice(Xpos)
                for pos in range(25,SCREEN_Y-25,25):
                    Ypos.append(pos)
                self.rect.top  = random.choice(Ypos)

                if self.rect not in wall.wallpos:
                    break

    def foodis(self,food):
        return self.foodtype == food