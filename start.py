               # -*- coding:utf-8 -*-
import pygame
import random
import sys
from src.snake  import  *
from src.food   import  *
from src.interface import *

#开始标志位
bStart = False

#初始选择界面
def startUI(screen, snake, clock):
    global bStart
    historyBestScore = get_rank()
    while not bStart:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_DOWN:
                    set_mode(True)
                if event.key == pygame.K_UP:
                    set_mode(False)
                if event.key == pygame.K_SPACE:
                    bStart = True
        
        show_background(screen)
        show_button(screen)
        show_record(screen)
        show_snake(screen, snake)
        show_grass(screen)
        clock.tick(10)
        pygame.display.update()

#显示分数
def showScores(screen, snake):
    if Casualmode():
        screen.blit(IMG_score, (SCREEN_X - 350, 50))
        show_number(screen, snake.score, (SCREEN_X - 200, 50))
    else:
        screen.blit(IMG_score, (80,SCREEN_Y - 220))
        screen.blit(IMG_speed, (80,SCREEN_Y - 170))
        screen.blit(IMG_body, (80,SCREEN_Y - 120))
        show_number(screen, snake.score, ( 230, SCREEN_Y - 220))
        show_number(screen, snake.speed, (230, SCREEN_Y - 170))
        show_number(screen, str(len(snake.body)), (230, SCREEN_Y - 120))

def show_skills(screen, skills):
    pos = 0
    for ele in skills:
        screen.blit(foods[ele], (pos, 0))
        pos += 25

#吃的食物判断
def foodJudge(food, snake):
    if food.foodis("rocket"):
        sod_eat3.play()
        snake.speed += 2
    elif food.foodis("snails"):
        sod_eat1.play()
        if snake.speed >= 4:
            snake.speed -=2
    elif food.foodis("bird"):
        sod_eat4.play()
        snake.score *= 2
    elif food.foodis("medicine"):
        sod_eat2.play()
        snake.addnode()
        snake.addnode()
    elif food.foodis("knife"):
        sod_eat5.play()
        snake.delnode()
        snake.delnode()
    elif food.foodis("blood"):
        sod_eat2.play()
        snake.skills.append("blood")
    else:   #其他食物
        sod_eat2.play()
        snake.score += 2 * snake.speed
        snake.addnode()
        pass
    snake.score+=(5 * snake.speed + len(snake.body))
    food.remove()
    food.setfoodtype(snake)
    if snake.score % 29 == 0:  #作为彩蛋加入
        food.foodtype = "bird"

def main():
    global bStart
    
    flagInit()
    pygame.init()                                   #游戏初始化
    screen_size = (SCREEN_X,SCREEN_Y)               #设置屏幕大小
    screen = pygame.display.set_mode(screen_size)   #设置显示屏幕
    pygame.display.set_caption('Flappy Snake')   #设置窗口标题名
    pygame.display.set_icon(title) 
    clock = pygame.time.Clock()                     #设置时钟对象
    isdead = False
    pygame.mixer.music.play(-1,0.0)

    # 蛇/食物
    snake = Snake()                                 #创建蛇对象
    food = Food()                                   #创建食物对象
    wall = Wall()#休闲模式才有墙
    startUI(screen,snake,clock)

    #不同模式的初始参数不同 
    if not Casualmode():
        food.allfoodtype.remove("snails")
        food.allfoodtype.remove("knife")
        food.allfoodtype.remove("blood")

    #游戏主循环
    while True:        
        fresh = snake.speed
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            if event.type == pygame.KEYDOWN:
                snake.changedirection(event.key)
                # 死后按space重新
                if event.key == pygame.K_SPACE and isdead:
                    bStart = False
                    return main()
                    
        #按下空格，加速
        if pygame.key.get_pressed()[pygame.K_SPACE] and not isdead:
            fresh = snake.speed * 2
                
        #显示背景
        show_background(screen)
        if Casualmode():
            show_wall(screen, wall)
            
        # 显示死亡文字
        isdead = snake.isdead(wall)
        if isdead:            
            #播放死亡音效、停止背景音乐、显示字样
            do_once(sod_over.play, "DeathWarning")
            pygame.mixer.music.stop() 
            show_overinfo(screen, snake.score, snake.speed, len(snake.body))
        else:
            snake.score+= (snake.speed // 5)
            snake.move()
  
        # 显示分数文字
        showScores(screen, snake)
        show_snake(screen, snake)
        show_grass(screen)  
        show_skills(screen, snake.skills)
            
        # 当食物rect与蛇头重合,吃掉,判断是哪一种食物，根据食物种类加分
        if food.rect == snake.body[0]:
            foodJudge(food, snake)
            
        # 食物投递
        food.set(wall)
        screen.blit(foods[food.foodtype], food.rect)       #食物放到屏幕上

        #游戏刷新速度/蛇运动速度
        pygame.display.update()
        clock.tick(fresh)
    
    
if __name__ == '__main__':
    main()
