# FlappySnake
**作品名称：** Flappy Snake

**作者ID：** 古凡

**项目简介：**
这是向曾经风靡一时的flappy bird致敬的作品，它以一种极简的操作让玩家体验极具虐心的感受，特借此机会制作一款类似风格的贪吃蛇版本。游戏分为2个模式，分别是 虐心的flabby模式 和 花样繁多的休闲模式 （休闲模式后续更新更多食物和玩法）。
- **flabby模式**  仅有两种食物，分别是加分瓢虫和加速火箭，吃下瓢虫加更多的分，吃下火箭得到更快的速度，速度由慢到快，直到你把持不住，结合背景音乐体验速度和激情，哦不，是虐心。保准活不过一首背景音乐的时间，来一较高下吧。
- **休闲模式** 添加多种玩法，现食物仅有三种，还有小南（蓝）墙，小蛇会撞墙和边界会死亡，当前因工作，后期还会继续添加更多墙类型和食物种类以及玩法。

**Gitee项目地址：**
https://gitee.com/angie1011/Adapted-game-FlappySnake.git

**演示视频：**
https://www.bilibili.com/video/BV1CV411q7SG/

**游戏截图：**
![游戏首页](https://images.gitee.com/uploads/images/2020/0806/194407_addb219f_2125029.png "082F98B7-ED91-4A26-8705-2BC10EF34FBC.png")
![游戏中](https://images.gitee.com/uploads/images/2020/0806/194428_2f5b698c_2125029.png "5CBAE71E-CC44-46A2-BFF7-149F6E422A35.png")
![游戏结束](https://images.gitee.com/uploads/images/2020/0806/194446_0d5bd949_2125029.png "482BCBF1-2E35-4A4F-8CD4-FB691AC3427F.png")

**安装教程/环境要求：**
本代码运行环境：windows10，  python3.6.8，  pygame1.9.6


**使用说明：**

直接双击**start.py**即可开始游戏，游戏按键：

	起始界面：上下选择模式，空格键开始
	游戏界面：上下左右控制蛇的方向，空格键加速
 _游戏记录保存于.data文件夹下的history文件中，若删除记录，将该文件删除即可。_ 

